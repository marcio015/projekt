/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>


#include <iostream>
#include<opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace cv;
using namespace std;
/* Header for class com_example_marcinb_opencvcamera_OpencvNativeClass */

#ifndef _Included_com_example_marcinb_opencvcamera_OpencvNativeClass
#define _Included_com_example_marcinb_opencvcamera_OpencvNativeClass
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_example_marcinb_opencvcamera_OpencvNativeClass
 * Method:    convertGray
 * Signature: (JJ)I
 */

 int toGray(Mat img, Mat& gray);
 void Add_salt_pepper_Noise(Mat &srcArr, float pa, float pb );
 void wave(const Mat &image, Mat &result);

/*
 * Class:     com_example_marcinb_opencvcamera_OpencvNativeClass
 * Method:    convertGray
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL Java_com_example_marcinb_opencvcamera_OpencvNativeClass_convertGray
  (JNIEnv *, jclass, jlong, jlong);

/*
 * Class:     com_example_marcinb_opencvcamera_OpencvNativeClass
 * Method:    Add_salt_pepper_Noise
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL Java_com_example_marcinb_opencvcamera_OpencvNativeClass_Add_1salt_1pepper_1Noise
  (JNIEnv *, jclass, jlong, jlong);

/*
 * Class:     com_example_marcinb_opencvcamera_OpencvNativeClass
 * Method:    Add_Wave
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL Java_com_example_marcinb_opencvcamera_OpencvNativeClass_Add_1Wave
  (JNIEnv *, jclass, jlong, jlong);

/*
 * Class:     com_example_marcinb_opencvcamera_OpencvNativeClass
 * Method:    Light
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_example_marcinb_opencvcamera_OpencvNativeClass_Light
  (JNIEnv *, jclass, jlong);

#ifdef __cplusplus
}
#endif
#endif
