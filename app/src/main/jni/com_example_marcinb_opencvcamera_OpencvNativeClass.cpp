

#include <com_example_marcinb_opencvcamera_OpencvNativeClass.h>


JNIEXPORT jint JNICALL Java_com_example_marcinb_opencvcamera_OpencvNativeClass_Light
  (JNIEnv *, jclass, jlong addrRgba){


    Mat& mRgb = *(Mat*)addrRgba;

      cv::Mat hsv;

       cv::cvtColor(mRgb, hsv, CV_BGR2HSV);

       // split the 3 channels into 3 images

       std::vector<cv::Mat> channels;

       cv::split(hsv,channels);

       // Value channel will be 255 for all pixels

       channels[2]= 255;

       // merge back the channels

       cv::merge(channels,hsv);

       // reconvert to BGR

       cv::Mat newImage;

       cv::cvtColor(hsv,mRgb,CV_HSV2BGR);

       //mRgb = newImage.clone();
      // mRgb = hsv.clone();
       return 1;


  }


JNIEXPORT jint JNICALL Java_com_example_marcinb_opencvcamera_OpencvNativeClass_convertGray
 (JNIEnv *, jclass, jlong addrRgba, jlong addrGrey){



  Mat& mRgb = *(Mat*)addrRgba;
  Mat mGray = *(Mat*)addrGrey;

  int conv;
  jint retVal;
  conv = toGray(mRgb,mGray);

   retVal = (jint)conv;

   return retVal;


  }




JNIEXPORT jint JNICALL Java_com_example_marcinb_opencvcamera_OpencvNativeClass_Add_1salt_1pepper_1Noise
 (JNIEnv *, jclass, jlong addrRgba, jlong addrSPN){

   Mat& mRgb = *(Mat*)addrRgba;
   Mat mSPN = *(Mat*)addrSPN;

  int conv;
  jint retVal;
/*  conv = toGray(mRgb,mSPN);

   retVal = (jint)conv;

   return retVal;*/

   Add_salt_pepper_Noise(mRgb,  50,240);
   return 1;


 }



 JNIEXPORT jint JNICALL Java_com_example_marcinb_opencvcamera_OpencvNativeClass_Add_1Wave
  (JNIEnv *, jclass, jlong addrRgba, jlong addrRST){

   Mat& mRgb = *(Mat*)addrRgba;
   Mat& mRST = *(Mat*)addrRST;

   wave( mRgb, mRST);
   return 1;

  }


void wave(const cv::Mat &image, cv::Mat &result) {

  // the map functions

  cv::Mat srcX(image.rows,image.cols,CV_32F);

  cv::Mat srcY(image.rows,image.cols,CV_32F);

  // creating the mapping

  for (int i=0; i<image.rows; i++) {

    for (int j=0; j<image.cols; j++) {

      // new location of pixel at (i,j)

      srcX.at<float>(i,j)= j; // remain on same column

                // pixels originally on row i are now

                // moved following a sinusoid

      srcY.at<float>(i,j)= i+5*sin(j/10.0);

    }

  }

  // applying the mapping

  cv::remap(image, result, srcX, srcY, cv::INTER_LINEAR);

}

 void Add_salt_pepper_Noise(Mat &srcArr, float pa, float pb )

 {
/*
 int i,j;

  for (int k=0; k<30000; k++) {

    //

    i= rand()%srcArr.cols;

    j= rand()%srcArr.rows;



      srcArr.at<cv::Vec3b>(j,i)[0]= 255;

      srcArr.at<cv::Vec3b>(j,i)[1]= 255;

      srcArr.at<cv::Vec3b>(j,i)[2]= 255;





  }*/



//2

 Mat saltpepper_noise = Mat::zeros(srcArr.rows, srcArr.cols,CV_8U);
 randu(saltpepper_noise,0,255);

 Mat black = saltpepper_noise < pa;
 Mat white = saltpepper_noise > pb;

 Mat saltpepper_img = srcArr.clone();
 saltpepper_img.setTo(255,white);
 saltpepper_img.setTo(0,black);


 srcArr=saltpepper_img.clone();


 //1
/*
 //cout<<srcArr.rows<<" "<<srcArr.cols<<endl;

     RNG rng;
     int amount1=srcArr.rows*srcArr.cols*pa;
     int amount2=srcArr.rows*srcArr.cols*pb;
*//*     Size w=srcArr.size();
     int amount1=w.height*w.width*pa;
     int amount2=w.height*w.width*pb;*//*

     for(int counter=0; counter<amount1; ++counter)
         {

            srcArr.at<uchar>(rng.uniform( 0,srcArr.rows), rng.uniform(0, srcArr.cols)) =0;

         }
      for (int counter=0; counter<amount2; ++counter)
          {
            srcArr.at<uchar>(rng.uniform(0,srcArr.rows), rng.uniform(0,srcArr.cols)) = 255;
          }*/
 }

   int toGray(Mat img, Mat& gray){
    cvtColor(img, gray, CV_RGBA2GRAY);
    if(gray.rows==img.rows && gray.cols==img.cols)
    return 1;
    return 2;

   }
