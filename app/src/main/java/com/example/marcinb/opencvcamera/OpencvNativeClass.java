package com.example.marcinb.opencvcamera;

/**
 * Created by MarcinB on 27.07.2017.
 */

public class OpencvNativeClass {
    public static native int convertGray(long matAddrRgba, long matAddrGray);
    public static native int Add_salt_pepper_Noise(long matAddrRgba, long matAddSPN);
    public static native int Add_Wave(long matAddrRgba, long matAddRST);
    public static native int Light(long matAddrRgba);
}
