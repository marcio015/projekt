package com.example.marcinb.opencvcamera;

import android.app.Activity;
import android.view.WindowManager;

/**
 * Created by michal on 24.11.2016.
 */
public class DebugHelper {
    public static void forceWakeUpAndShowScreenWhenStartApp(Activity activity) {
        if (BuildConfig.DEBUG) {
            // These flags cause the device screen to turn on (and bypass screen guard if possible) when launching.
            // This makes it easy for developers to test the app launch without needing to turn on the device
            // each time and without needing to enable the "Stay awake" option.
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                    | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        }
    }
}
