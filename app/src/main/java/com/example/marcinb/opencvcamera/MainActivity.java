package com.example.marcinb.opencvcamera;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.List;
import java.util.Vector;

/**
 * Uzyty język w tłumaczeniu tego kodu: polski, z częściowym pominięciem znaków diakrytycznych
 * <p>
 * Pomocne klawisze skrótów:
 * <p>
 * ctrl + q z kursorem na metodzie = dokumentacja
 * ctrl + p z kursorem na metodzie miedzy nawiasami, tam gdzie parametry = podpowiedz jakie parametry mozna dodac
 */
public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2, AdapterView.OnItemSelectedListener {
    private static final String TAG = "MainActivity";
    Mat mRgba;
    Mat mGray;
    static {
        System.loadLibrary("MyOpencvLibs");
    }
    private JavaCameraView javaCameraView;

    /**
     * listener dla obsługi kiedy kamera jest włączona
     */
    private BaseLoaderCallback mloaderCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            super.onManagerConnected(status);
            switch (status) {
                case BaseLoaderCallback.SUCCESS: {
                    javaCameraView.enableView();
                    break;
                }
                default: {
                    super.onManagerConnected(status);
                    break;
                }
            }
        }
    };

    /**
     * Element górny do wyboru opcji, obecnie są 3 tryby - brak efektu, szaro-odcieniowość, rozmycie.
     * Posiada adapter z listą opcji w pliku arrays.xml - deklaracja listy jest w xmlu layoutu tej aktywności
     */
    private Spinner spinner;

    /**
     * Parametr do pobierania lub zapisywania wybranej opcji ze spinnera
     */
    private CameraTypeSelected cameraTypeSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DebugHelper.forceWakeUpAndShowScreenWhenStartApp(this);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        javaCameraView = (JavaCameraView) findViewById(R.id.java_camera_view);
        javaCameraView.setVisibility(SurfaceView.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (OpenCVLoader.initDebug()) {
            Log.d(TAG, "Opencv succesfull loaded");
            mloaderCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);

        } else {
            Log.d(TAG, "OpenCv not loaded");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mloaderCallBack);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height,width, CvType.CV_8UC4);
        mGray = new Mat(height,width, CvType.CV_8UC1);
    }

    @Override
    public void onCameraViewStopped() {
        //... nic do zrobienia
    }

    /**
     * Tutaj klatkowane są przechwycone obrazy z kamery
     */
    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        switch (cameraTypeSelected) {
            case NO_EFFECT:
                /*Mat input = inputFrame.rgba();
                Mat hsl = input.clone();
                Imgproc.cvtColor( input,hsl, 40);
                Vector<Mat> channels = new Vector<>();
               // channels.add(255);
             Core.split(hsl,channels);

                channels.add(2,input);
                Core.merge(channels,hsl);
                Mat result2 = input.clone();
                Imgproc.cvtColor(hsl,result2,54);
                return hsl;*/
                return inputFrame.rgba();
            case GRAYSCALE:
                return inputFrame.gray();
            case BLUR:
                Mat src = inputFrame.rgba();//robimy blur na kolorze, więc pobieramy wpierw kolorową ramke
                Mat dst = src.clone();//kopia, żeby mieć wymiary takie same co 'source' = bazowa ramka/Mat
                Size blurSize = new Size(20, 20);//rozmiar wokół jakiego rozmiaru sąsiadów piksla - per piksel (?)
                Imgproc.blur(src, dst, blurSize);//zapisuje efekt - tutaj blur - do dst
                src.release();//zwalniamy uzycie src, gdyż nie będziemy nad nim pracować - 'destination' jest wynikiem końcowym
                return dst;
            case GAUSSS:

                Mat src2 = inputFrame.rgba();//robimy blur na kolorze, więc pobieramy wpierw kolorową ramke
                Mat dst2 = src2.clone();//kopia, żeby mieć wymiary takie same co 'source' = bazowa ramka/Mat
                Size blurSize2 = new Size(15, 15);//rozmiar wokół jakiego rozmiaru sąsiadów piksla - per piksel (?)
                Imgproc.GaussianBlur(src2, dst2, blurSize2,50);//zapisuje efekt - tutaj blur - do dst
                src2.release();//zwalniamy uzycie src, gdyż nie będziemy nad nim pracować - 'destination' jest wynikiem końcowym
                return dst2;
            case JASNOSC:
                Mat src4 = inputFrame.rgba();
                Mat dsc4 = src4.clone();
                src4.convertTo(dsc4,src4.type(),1,-100);
                src4.release();
                return dsc4;
            case GRAYSCALE_NATIVE:
                mRgba = inputFrame.rgba();
                OpencvNativeClass.convertGray(mRgba.getNativeObjAddr(), mGray.getNativeObjAddr());
                return mGray;
            case SNOW:
                mRgba = inputFrame.rgba();
                OpencvNativeClass.Add_salt_pepper_Noise(mRgba.getNativeObjAddr(), mGray.getNativeObjAddr());
                return mRgba;


             /*   Mat rgba = inputFrame.rgba();
                Mat result = rgba.clone();
                OpencvNativeClass.Add_Wave(rgba.getNativeObjAddr(),result.getNativeObjAddr());
                return result;*/
            case WAVE:
                Mat rgba = inputFrame.rgba();
                Mat result = rgba.clone();
                OpencvNativeClass.Add_Wave(rgba.getNativeObjAddr(),result.getNativeObjAddr());
                return result;
            case LIGHT1:
                mRgba = inputFrame.rgba();
                OpencvNativeClass.Light(mRgba.getNativeObjAddr());
                return mRgba;

            default:
                throw new IllegalArgumentException("Nie ustalone tego typu, typ: " + cameraTypeSelected.name());
        }
    }

    /**
     * Spinner listeners
     */

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0://no effect
                cameraTypeSelected = CameraTypeSelected.NO_EFFECT;
                break;
            case 1://grayscale
                cameraTypeSelected = CameraTypeSelected.GRAYSCALE;
                break;
            case 2://blur
                cameraTypeSelected = CameraTypeSelected.BLUR;
                break;
            case 3://GAUSSS
                cameraTypeSelected = CameraTypeSelected.GAUSSS;
                break;
            case 4://JASNOSC
                cameraTypeSelected = CameraTypeSelected.JASNOSC;
                break;
            case 5://GRAYSCALE_NATIVE
                cameraTypeSelected = CameraTypeSelected.GRAYSCALE_NATIVE;
                break;
            case 6://SNOW
                cameraTypeSelected = CameraTypeSelected.SNOW;
                break;
            case 7://WAVE
                cameraTypeSelected = CameraTypeSelected.WAVE;
                break;
            case 8://LIGHT1
                cameraTypeSelected = CameraTypeSelected.LIGHT1;
                break;

            default:
                throw new IllegalArgumentException("Nie ustalone tej pozycji, pozycja: " + position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //...
    }

    /**
     * definicja efektów, zwykły enum, uzywany do sprawdzania wyboru, nic wiecej
     */
    enum CameraTypeSelected {
        NO_EFFECT, GRAYSCALE, BLUR,GAUSSS,JASNOSC,GRAYSCALE_NATIVE,SNOW,WAVE,LIGHT1
    }
}
